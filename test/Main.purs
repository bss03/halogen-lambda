module Test.Main where

import Prelude

import Control.Monad.Gen (unfoldable)
import Effect (Effect)
import Effect.Class.Console (log)
import Lexeme (Lexeme, genLexeme, renderParse)
import Test.QuickCheck (quickCheck)
import Test.QuickCheck.Gen (Gen)
import Token (Token, renderParseLexemes, genToken)

main :: Effect Unit
main = do
  quickCheck $ renderParse <$> (genLexeme :: Gen Lexeme)
  quickCheck $ renderParseLexemes <$> (unfoldable (genToken genLexeme :: Gen Token))
  log "Yay!"
