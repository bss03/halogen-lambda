module Lexeme where

import Prelude hiding (div)

import Control.Monad.Gen (class MonadGen)
import Control.Monad.Gen as Gen
import Control.Monad.Rec.Class (class MonadRec)
import Data.Array (fromFoldable, some, cons)
import Data.Array.NonEmpty.Internal (NonEmptyArray(..))
import Data.CodePoint.Unicode (isLetter)
import Data.Either (either)
import Data.Foldable (class Foldable)
import Data.Foldable as Foldable
import Data.Generic.Rep (class Generic)
import Data.Show.Generic (genericShow)
import Data.String (codePointFromChar)
import Data.String.CodeUnits (fromCharArray, singleton)
import Data.String.Gen (genAlphaString)
import Data.Tuple (Tuple(..))
import Halogen (ClassName(..))
import Halogen.HTML (HTML, div, span, text)
import Halogen.HTML.Properties (class_, classes)
import Parsing (Parser, fail, runParser)
import Parsing.Combinators (try)
import Parsing.String (anyChar, anyCodePoint, char, satisfy)

data Lexeme
  = Lambda
  | Identifier String
  | Dot
  | OpenParen
  | CloseParen

derive instance eqLexeme :: Eq Lexeme
derive instance genericLexeme :: Generic Lexeme _

instance Show Lexeme where
  show = genericShow

genLexeme :: forall g. MonadRec g => MonadGen g => g Lexeme
genLexeme = Gen.oneOf
  ( NonEmptyArray $ cons
      (Identifier <$> genAlphaString)
      (map pure [ Lambda, Dot, OpenParen, CloseParen ])
  )

identifier :: Parser String String
identifier = try $ fromCharArray <$> some
  (satisfy (\c -> isLetter (codePointFromChar c)))

lexeme :: Parser String Lexeme
lexeme =
  Foldable.oneOf
    [ Lambda <$ char '\\'
    , Dot <$ char '.'
    , OpenParen <$ char '('
    , CloseParen <$ char ')'
    , Identifier <$> identifier
    , do
       badChar <- anyChar
       fail $ "Bad input character: " <> show badChar
    , do
       badCode <- anyCodePoint
       fail $ "Bad input code point (!): " <> show badCode
    ]

renderLexeme :: Lexeme -> String
renderLexeme Lambda = singleton '\\'
renderLexeme (Identifier s) = s
renderLexeme Dot = singleton '.'
renderLexeme OpenParen = singleton '('
renderLexeme CloseParen = singleton ')'

-- | QuickCheck property; semantically: parse . render = id
renderParse :: Lexeme -> Boolean
renderParse x =
  either (const false) (x == _) $ renderLexeme x `runParser` lexeme

displayLexeme :: forall slot action. Lexeme -> HTML slot action
displayLexeme lex = span
  [ classes $ [ cLexeme "display", cType ty ] ]
  [ text txt ]
  where
  Tuple ty txt =
    case lex of
      Lambda -> Tuple "lambda" "\\"
      Identifier id -> Tuple "identifier" id
      Dot -> Tuple "dot" "."
      OpenParen -> Tuple "openParen" "("
      CloseParen -> Tuple "closeParen" ")"
  cLexeme c = ClassName $ "lexeme_" <> c
  cType c = cLexeme $ "tag_" <> c

displayLexemes ::
  forall f slot action. Functor f => Foldable f => f Lexeme -> HTML slot action
displayLexemes ls =
  div
    [ class_ $ ClassName "lexemes_display" ]
    (fromFoldable (displayLexeme <$> ls))
