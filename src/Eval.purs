module Eval where

import Prelude hiding (div)

import Bound (instantiate1)
import Bounded (BExpr(..), Lambda, displayBExpr)
import Control.Monad.State.Class (get, put)
import Data.Bifunctor (bimap)
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..))
import Data.Show.Generic (genericShow)
import Data.Tuple (Tuple(..), snd)
import Halogen (ClassName(..), Component, HalogenM, Slot, mkComponent, mkEval, query)
import Halogen.HTML (div, slot_, span, text)
import Halogen.HTML.Elements (a)
import Halogen.HTML.Events (onClick)
import Halogen.HTML.Properties (class_)
import Type.Proxy (Proxy(..))

data SmallStep
  = Beta
  | Function SmallStep

derive instance Generic SmallStep _

instance Show SmallStep where
  show s = genericShow s

displaySmallStep :: SmallStep -> String
displaySmallStep = show

smallStep ::
  forall identifier.
  BExpr identifier ->
  Maybe (Tuple SmallStep (BExpr identifier))
smallStep (Application a) = case a.function of
  Abstraction l -> Just (Tuple Beta $ instantiate1 a.argument l.body)
  x -> bimap Function (Application <<< a { function = _ }) <$> smallStep x
smallStep (Abstraction _) = Nothing
smallStep (FreeVariable _) = Nothing

newtype StepperQuery :: Type -> Type
newtype StepperQuery a = NewStart (BExpr Void)

type StepperState = Maybe
  (Maybe (Tuple (Tuple SmallStep (BExpr Void)) Boolean))

type Stepper m = Component StepperQuery Unit (Lambda Void) m

aslNextStepper :: Proxy "nextStepper"
aslNextStepper = Proxy

type StepperComponents = (nextStepper :: Slot StepperQuery (Lambda Void) Unit)

stepper :: forall m. Stepper m
stepper = mkComponent
  { initialState: const Nothing
  , render
  , eval: mkEval
      { initialize: Nothing
      , receive: const Nothing
      , handleAction
      , handleQuery
      , finalize: Nothing
      }
  }
  where
  render Nothing = text "No expression to evaluate, yet."
  render (Just Nothing) = text "Expression in normal form (or stuck)."
  render (Just (Just (Tuple (Tuple step next) renderNext))) =
    div [ class_ $ ClassName "stepper" ] $
      [ span
          [ class_ $ ClassName "stepper-prose" ]
          [ text "Next: "
          , a [ onClick (const unit) ] [ text (displaySmallStep step) ]
          ]
      ] <>
        if renderNext then
          [ div
              [ class_ $ ClassName "stepper-expr" ]
              [ displayBExpr absurd next
              , slot_ aslNextStepper unit stepper unit
              ]
          ]
        else mempty

  handleAction unitIn = do
    state <- get
    case state of
      Just (Just (Tuple next false)) -> do
        put (Just (Just (Tuple next true)))
        void $ query aslNextStepper unit (NewStart $ snd next)
        pure unitIn
      _ -> pure unitIn

  handleQuery ::
    forall a.
    StepperQuery a ->
    HalogenM StepperState Unit StepperComponents (Lambda Void) m (Maybe a)
  handleQuery (NewStart expr) = do
    put <<< Just $ case smallStep expr of
      Nothing -> Nothing
      Just next -> Just (Tuple next false)
    pure Nothing
