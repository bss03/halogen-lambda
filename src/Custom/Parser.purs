module Custom.Parser (sepByLazy) where

import Prelude

import Data.List (List(..), many)
import Parsing (ParserT)
import Parsing.Combinators (try)

-- | 'try' on the many target
sepByLazy ::
  forall s m elem sep.
  Monad m =>
  ParserT s m elem ->
  ParserT s m sep ->
  ParserT s m (List elem)
sepByLazy elemp sepp = do
  h <- elemp
  t <- many (try (sepp *> elemp))
  pure $ Cons h t
