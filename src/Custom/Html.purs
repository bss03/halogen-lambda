module Custom.Html (overlay) where

import Prelude hiding (div)

import Halogen (ClassName(..))
import Halogen.HTML (HTML, div)
import Halogen.HTML.Properties (class_, classes)

overlay ::
  forall slot action.
  Array ClassName ->
  Array (HTML slot action) ->
  HTML slot action
overlay panel_classes contents =
  div
    [ class_ $ ClassName "overlay" ]
    [ div
        [ classes panel_classes ]
        [ div [ class_ $ ClassName "overlay_content" ] contents ]
    ]
