module Main where

import Prelude hiding (div)

import Bound (closed)
import Bounded (BoundsCheckerQuery(..), Lambda, boundsChecker)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Class.Console (log)
import Eval (StepperQuery(..), stepper)
import Expr (ParserQuery(Parse), parserComponent)
import Halogen (Component, ComponentHTML, HalogenM, HalogenQ, Slot, mkComponent, mkEval, query)
import Halogen.Aff (runHalogenAff, awaitBody)
import Halogen.HTML (HTML, div, div_, slot_, textarea, PropName(PropName))
import Halogen.HTML.Events (onValueInput)
import Halogen.HTML.Properties (id, prop)
import Halogen.VDom.Driver (runUI)
import Token (TokenizerQuery(..), tokenizer)
import Type.Proxy (Proxy(..))

initialValue :: String
initialValue = "(\\x.x x)\\x.x x"

inputTA :: forall w. HTML w String
inputTA = textarea
  [ prop (PropName "value") initialValue, onValueInput identity ]

aslTokenizer :: Proxy "tokenizer"
aslTokenizer = Proxy

aslParser :: Proxy "parser"
aslParser = Proxy

aslBoundsChecker :: Proxy "boundsChecker"
aslBoundsChecker = Proxy

aslStepper :: Proxy "stepper"
aslStepper = Proxy

type AppComponents =
  ( tokenizer :: Slot TokenizerQuery Void Unit
  , parser :: Slot ParserQuery Void Unit
  , boundsChecker :: Slot BoundsCheckerQuery Void Unit
  , stepper :: Slot StepperQuery (Lambda Void) Unit
  )

renderApp :: forall s m. s -> ComponentHTML String AppComponents m
renderApp = const $
  div
    [ id "app" ]
    [ div_ [ inputTA ]
    , slot_ aslTokenizer unit tokenizer unit
    , slot_ aslParser unit parserComponent unit
    , slot_ aslBoundsChecker unit boundsChecker unit
    , slot_ aslStepper unit stepper unit
    ]

evalApp ::
  forall query state input output m.
  (HalogenQ query String input) ~> (HalogenM state String AppComponents output m)
evalApp =
  mkEval
    { finalize: Nothing
    , receive: const Nothing
    , handleAction
    , handleQuery: const (pure Nothing)
    , initialize: Just initialValue
    }
  where
  handleAction str = do
    mToks <- map join $ query aslTokenizer unit (Tokenize str Just (const Nothing))
    case mToks of
      Nothing -> pure unit
      Just toks -> do
        mExpr <- query aslParser unit (Parse toks identity)
        case mExpr of
          Nothing -> pure unit
          Just expr -> do
             bexpr <- map join $ query aslBoundsChecker unit (BoundsCheck expr closed)
             case bexpr of
                 Nothing -> pure unit
                 Just init -> void $ query aslStepper unit (NewStart init)

app :: forall query input output m. Component query input output m
app =
  mkComponent
    { initialState: const ""
    , render: renderApp
    , eval: evalApp
    }

main :: Effect Unit
main = runHalogenAff do
  body <- awaitBody
  log "Ready to runUI"
  runUI app unit body
