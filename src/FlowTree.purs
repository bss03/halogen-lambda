module FlowTree (flowTree, flowTreeM, HtmlTreeF(..)) where

import Prelude hiding (div)

import Data.Foldable (class Foldable)
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype, un, unwrap, wrap)
import Data.Traversable (class Traversable)
import Data.Tuple (Tuple(..))
import Halogen.HTML.Core (HTML)
import Halogen.HTML.Elements (div)
import Halogen.HTML.Properties (class_)
import Matryoshka (class Corecursive, ana, anaM)
import Web.HTML.Common (ClassName(ClassName))

newtype HtmlTreeF w i a = MkTreeF
  { rootContent :: Array (HTML w i)
  , links :: Array (Tuple (Maybe (Array (HTML w i))) a)
  }

derive instance Functor (HtmlTreeF w i)
derive instance Foldable (HtmlTreeF w i)
derive instance Traversable (HtmlTreeF w i)

newtype FlowTree w i = FlowTree (HTML w i)

derive instance Newtype (FlowTree w i) _

instance Corecursive (FlowTree w i) (HtmlTreeF w i) where
  embed (MkTreeF t) =
    wrap $ div [ class_ $ ClassName "flowtree" ]
      [ div [ class_ $ ClassName "flowtree-node" ] t.rootContent
      , div [ class_ $ ClassName "flowtree-links" ] (map link t.links)
      ]
    where
    link (Tuple mLabel content) =
      div [ class_ $ ClassName "flowtree-link" ] linkContents
      where
      linkContents = case mLabel of
        Nothing -> [ unwrap content ]
        Just label ->
          [ div [ class_ $ ClassName "flowtree-link-label" ] label, unwrap content ]

flowTree :: forall a w i. (a -> HtmlTreeF w i a) -> a -> HTML w i
flowTree alg = un FlowTree <<< ana alg

flowTreeM :: forall a w i m. Monad m => (a -> m (HtmlTreeF w i a)) -> a -> m (HTML w i)
flowTreeM algm = map (un FlowTree) <<< anaM algm
