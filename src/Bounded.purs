module Bounded where

import Prelude hiding (div)

import Bound (Scope(Scope), Var(F, B), fromScope, hoistScope, subst, unScope)
import Data.Coyoneda (Coyoneda, hoistCoyoneda, liftCoyoneda, lowerCoyoneda)
import Data.Foldable (class Foldable, foldMap, foldlDefault, foldrDefault)
import Data.Generic.Rep (class Generic)
import Data.Identity (Identity(Identity))
import Data.Maybe (Maybe(..), maybe)
import Data.Newtype (un, unwrap)
import Data.Show.Generic (genericShow)
import Data.Traversable (class Traversable, sequence, traverseDefault)
import Expr (Expr)
import Expr as Expr
import FixFunctor (class HFunctor, hmap)
import Halogen
  ( ClassName(ClassName)
  , HalogenM
  , Component
  , mkComponent
  , mkEval
  , put
  )
import Halogen.HTML (HTML, text, div, span)
import Halogen.HTML.Properties (class_)
import Ran (lower')
import Ran as Ran

type Lambda a = { variableName :: String, body :: Scope Unit BExpr a }

data BExpr a
  = Application { function :: BExpr a, argument :: BExpr a }
  | Abstraction (Lambda a)
  | FreeVariable a

data BExprF r a
  = AppF { function :: r a, argument :: r a }
  | AbsF { variableName :: String, body :: Scope Unit (Coyoneda r) a }
  | VarF a

derive instance Functor f => Functor (BExprF f)

instance HFunctor BExprF where
  hmap f = h
    where
    h (AppF a) = AppF { function: f a.function, argument: f a.argument }
    h (AbsF l) = AbsF l { body = hoistScope (hoistCoyoneda f) l.body }
    h (VarF x) = VarF x

type HAlg :: forall k. ((k -> Type) -> k -> Type) -> (k -> Type) -> Type
type HAlg f g = f g ~> g

type HCoalg :: forall k. ((k -> Type) -> k -> Type) -> (k -> Type) -> Type
type HCoalg f g = g ~> f g

class Nested ::
  forall k. (k -> Type) -> ((k -> Type) -> k -> Type) -> Constraint
class HFunctor f <= Nested t f | t -> f where
  hproject :: HCoalg f t
  hembed :: HAlg f t

instance Nested BExpr BExprF where
  hproject (Application a) = AppF a
  hproject (Abstraction l) = AbsF l { body = hoistScope liftCoyoneda l.body }
  hproject (FreeVariable x) = VarF x

  hembed (AppF a) = Application a
  hembed (AbsF l) = Abstraction l { body = hoistScope lowerCoyoneda l.body }
  hembed (VarF x) = FreeVariable x

hfold :: forall r t f. Nested t f => HAlg f r -> t ~> r
hfold a = f
  where
  f :: t ~> r
  f x = a <<< hmap f $ hproject x

-- | `hfold a . hbuild b = b a`
hbuild :: forall r t f. Nested t f => (forall x. HAlg f x -> r ~> x) -> r ~> t
hbuild f = f hembed

hunfold :: forall r t f. Nested t f => HCoalg f r -> (r ~> t)
hunfold c = u
  where
  u :: r ~> t
  u x = hembed <<< hmap u $ c x

-- | `hdestroy d . hunfold c = c d`
hdestroy ::
  forall r t f. Nested t f => (forall x. HCoalg f x -> x ~> r) -> t ~> r
hdestroy u = u hproject

newtype Ran f g a = MkRan { unRan :: Ran.Ran f g a }

derive instance Functor (Ran f g)

gfold ::
  forall t f c r i o. Nested t f => HAlg f (Ran c r) -> t i -> (i -> c o) -> r o
gfold a = u <<< hfold a
  where
  u (MkRan r) = r.unRan

class FFunctor f where
  fmap :: forall a b g. Functor g => (a -> b) -> f g a -> f g b

instance FFunctor BExprF where
  fmap f = x
    where
    x (AppF a) = AppF { function: map f a.function, argument: map f a.argument }
    x (AbsF l) = AbsF l { body = map f l.body }
    x (VarF y) = VarF $ f y

lowerRan :: forall f g. Applicative f => Ran f g ~> g
lowerRan (MkRan r) = lower' r.unRan

mapNested :: forall t f a b. Nested t f => FFunctor f => (a -> b) -> t a -> t b
mapNested f x = gfold mapAlg x (Identity <<< f)
  where
  mapAlg :: HAlg f (Ran Identity t)
  mapAlg t = MkRan
    { unRan: \e -> hembed (hmap lowerRan (fmap (un Identity <<< e) t)) }

derive instance Generic (BExpr a) _
derive instance Functor BExpr

instance Apply BExpr where
  apply = ap

instance Applicative BExpr where
  pure = FreeVariable

instance Bind BExpr where
  bind (Application app) f = Application
    { function: bind app.function f, argument: bind app.argument f }
  bind (Abstraction abs) f = Abstraction
    { variableName: abs.variableName, body: subst f abs.body }
  bind (FreeVariable x) f = f x

instance Monad BExpr

instance Foldable BExpr where
  foldMap f = fm
    where
    fm (Application a) = fm a.function <> fm a.argument
    fm (Abstraction l) = foldMap f l.body
    fm (FreeVariable x) = f x
  foldl s = foldlDefault s
  foldr s = foldrDefault s

instance Traversable BExpr where
  sequence (Application a) = ado
    function <- sequence a.function
    argument <- sequence a.argument
    in Application { function, argument }
  sequence (Abstraction l) = ado
    body <- sequence l.body
    in Abstraction l { body = body }
  sequence (FreeVariable x) = ado
    y <- x
    in FreeVariable y
  traverse act = traverseDefault act

instance Show a => Show (BExpr a) where
  show x = genericShow x

boundsCheckClosed :: Expr -> BExpr String
boundsCheckClosed x = unwrap $ boundsCheck Identity x

boundsCheck ::
  forall identifier m.
  Monad m =>
  (String -> m identifier) ->
  Expr ->
  m (BExpr identifier)
boundsCheck context = bc
  where
  bc (Expr.Application f x) = do
    function <- bc f
    argument <- bc x
    pure $ Application { function, argument }
  bc (Expr.Abstraction v b) = do
    body <- Scope <$> boundsCheck newContext b
    pure $ Abstraction { variableName: v, body }
    where
    newContext x | v == x = pure (B unit)
    newContext x = (\y -> F $ FreeVariable y) <$> context x
  bc (Expr.Variable x) = FreeVariable <$> context x

showBExpr ::
  forall identifier m.
  Monad m =>
  (identifier -> m String) ->
  BExpr identifier ->
  m String
showBExpr showIdentifier = s (const showIdentifier) false
  where
  s ::
    forall id2. (Boolean -> id2 -> m String) -> Boolean -> BExpr id2 -> m String
  s renderFree = s2
    where
    s2 needSelfTerminated = s3
      where
      s3 (FreeVariable x) = renderFree needSelfTerminated x
      s3 (Abstraction l) = terminateIfNeeded <$> do
        shownBody <- s (flip showVar) false (fromScope l.body)
        pure $ "\\" <> l.variableName <> "." <> shownBody
        where
        terminateIfNeeded x = if needSelfTerminated then "(" <> x <> ")" else x
        showVar (B _unit) = const $ pure l.variableName
        showVar (F x) = flip renderFree x
      s3 (Application a) = do
        shownFunction <- s2 true a.function
        shownArgument <- s3 a.argument
        pure $ shownFunction <> " " <> shownArgument

displayBExpr ::
  forall identifier slot action.
  (identifier -> HTML slot action) ->
  BExpr identifier ->
  HTML slot action
displayBExpr displayFree = d
  where
  d (FreeVariable x) = displayFree x
  d (Abstraction l) =
    div
      [ class_ $ ClassName "bounded-abstraction" ]
      [ span [ class_ $ ClassName "bounded-scope-label" ]
          [ text $ "Scope of " <> l.variableName ]
      , div [ class_ $ ClassName "bounded-scope" ]
          [ displayBExpr displayVar $ unScope l.body ]
      ]
    where
    displayVar (B _unit) =
      span [ class_ $ ClassName "bounded-variable" ] [ text l.variableName ]
    displayVar (F n) = d n
  d (Application a) =
    div
      [ class_ $ ClassName "bounded-application" ]
      [ div [ class_ $ ClassName "bounded-function" ] [ d a.function ]
      , div [ class_ $ ClassName "bounded-argument" ] [ d a.argument ]
      ]

data BoundsCheckerQuery a = BoundsCheck Expr (BExpr String -> a)

boundsChecker :: forall output m. Component BoundsCheckerQuery Unit output m
boundsChecker = mkComponent
  { initialState: const Nothing
  , render: \mExpr -> div [ class_ $ ClassName "bounds-checker" ] $ maybe []
      ( pure <<< displayBExpr
          ( \unbound -> span
              [ class_ $ ClassName "unbound-variable" ]
              [ text unbound ]
          )
      )
      mExpr
  , eval: mkEval
      { initialize: Nothing
      , receive: const Nothing
      , handleAction: const (pure unit)
      , handleQuery
      , finalize: Nothing
      }
  }
  where
  handleQuery ::
    forall a action slots.
    BoundsCheckerQuery a ->
    HalogenM (Maybe (BExpr String)) action slots output m (Maybe a)
  handleQuery (BoundsCheck expr onBound) = do
    put $ Just bound
    pure <<< Just $ onBound bound
    where
    bound = boundsCheckClosed expr
