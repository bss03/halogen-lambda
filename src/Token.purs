module Token (Token(..), TokenizerQuery(..), tokenizer, renderParseLexemes, genToken) where

import Prelude hiding (div)

import Control.Monad.Gen (class MonadGen, chooseInt)
import Custom.Html (overlay)
import Custom.Parser (sepByLazy)
import Data.Either (Either(..), either)
import Data.Foldable (foldr)
import Data.List (List(..))
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype, over, unwrap)
import Halogen (ClassName(..), Component, HalogenM, mkComponent, mkEval, modify_, put)
import Halogen.HTML (text, div)
import Halogen.HTML.Properties (class_)
import Lexeme (Lexeme(..), displayLexemes, lexeme, renderLexeme)
import Parsing (ParseError, ParserT, Position(Position), position, runParser)
import Parsing.String (eof)
import Parsing.String.Basic (skipSpaces, whiteSpace)

newtype Token = Token
  { lexeme :: Lexeme
  , start :: Position
  , end :: Position
  }

derive instance ntToken :: Newtype Token _

genToken :: forall m. MonadGen m => m Lexeme -> m Token
genToken lexeme = ado
  start <- gp
  lex <- lexeme
  end <- gp
 in Token { start, end, lexeme: lex }
 where
   gp = ado
    column <- chooseInt 0 125
    index <- chooseInt 1 50000
    line <- chooseInt 1 2500
    in Position { column, line, index }

token :: forall s m. Monad m => ParserT s m Lexeme -> ParserT s m Token
token lexeme = ado
  start <- position
  lex <- lexeme
  end <- position
  in
    Token
      { start
      , lexeme: lex
      , end
      }

tokens ::
  forall m. Monad m => ParserT String m Lexeme -> ParserT String m (List Token)
tokens lexeme = sepByLazy (token lexeme) whiteSpace

renderTokens :: List Token -> String
renderTokens toks = foldr alg (const "") (map (_.lexeme <<< unwrap) toks) false
  where
  alg (Identifier x) n s = (if s then (" " <> x) else x) <> n true
  alg l n _s = renderLexeme l <> n false

renderParseLexemes :: List Token -> Boolean
renderParseLexemes toks =
  either (const false) (eqOnLexeme toks) $
    runParser
      (renderTokens toks)
      (tokens lexeme)
  where
  eqOnLexeme Nil Nil = true
  eqOnLexeme (Cons (Token x) xs) (Cons (Token y) ys) =
    x.lexeme == y.lexeme && eqOnLexeme xs ys
  eqOnLexeme _ _ = false

newtype TokenizerState = TokenizerState
  { parseError :: Maybe ParseError
  , lastTokenize :: List Token
  }

derive instance ntTokenizerState :: Newtype TokenizerState _

data TokenizerQuery a = Tokenize String (List Token -> a) (ParseError -> a)

tokQuery ::
  forall a action slots output m.
  TokenizerQuery a ->
  HalogenM TokenizerState action slots output m (Maybe a)
tokQuery (Tokenize str good bad) =
  case runParser str (skipSpaces *> tokens lexeme <* skipSpaces <* eof) of
    Left pe -> do
      modify_ (over TokenizerState (_ { parseError = Just pe }))
      pure $ Just $ bad pe
    Right toks -> do
      put $ TokenizerState { parseError: Nothing, lastTokenize: toks }
      pure $ Just $ good toks

tokenizer :: forall output m. Component TokenizerQuery Unit output m
tokenizer = mkComponent
  { initialState: const $ TokenizerState
      { parseError: Nothing, lastTokenize: Nil }
  , render
  , eval: mkEval
      { initialize: Nothing
      , receive: const Nothing
      , handleAction: const (pure unit)
      , handleQuery: tokQuery
      , finalize: Nothing
      }
  }
  where
  render (TokenizerState st) = div [ class_ $ ClassName "tokenizer" ] $
    case st.parseError of
      Nothing -> [ renderToks ]
      Just pe ->
        [ overlay [ ClassName "tokenizer_error" ] [ text (show pe) ]
        , renderToks
        ]
    where
    renderToks = displayLexemes $ map (\(Token t) -> t.lexeme) st.lastTokenize
