module Expr where

import Prelude hiding (between, div)

import Control.Lazy (fix)
import Custom.Html (overlay)
import Data.Array.NonEmpty (some, foldl1)
import Data.Either (Either(..))
import Data.Foldable (oneOf)
import Data.Generic.Rep (class Generic)
import Data.List (List)
import Data.Maybe (Maybe(..), maybe)
import Data.Newtype (class Newtype, over)
import Data.Show.Generic (genericShow)
import Data.Tuple (Tuple(..))
import FlowTree (HtmlTreeF(..), flowTree)
import Halogen (ClassName(..), HalogenM, mkEval, modify_, put)
import Halogen.Component (Component, mkComponent)
import Halogen.HTML (HTML, div, text)
import Halogen.HTML.Properties (class_)
import Lexeme (Lexeme(..))
import Parsing (ParseError, Parser, fail, runParser)
import Parsing.Combinators (between, optionMaybe, try, notFollowedBy)
import Parsing.Token (token)
import Token (Token(..))

data Expr
  = Application Expr Expr
  | Abstraction String Expr
  | Variable String

derive instance Eq Expr
derive instance Generic Expr _

instance Show Expr where
  show e = genericShow e

nextToken :: Parser (List Token) Token
nextToken = token (\(Token t) -> t.end)

expr :: Parser (List Token) Expr -> Parser (List Token) Expr
expr e = oneOf
  [ Abstraction <$> (lambda *> identifier) <*> (dot *> e)
  , app
  , fail "Expected lambda, application, or identifier"
  ]
  where
  app = do
    fs <- some (try atom)
    let i = foldl1 Application fs
    t <- optionMaybe (try e)
    pure $ case t of
      Nothing -> i
      Just l -> Application i l
  atom = oneOf
    [ Variable <$> identifier
    , between openParen closeParen e
    , fail "Expected an atom (identifier or parenthesized expression)"
    ]
  lexeme l = try $ do
    Token t <- nextToken
    when (t.lexeme /= l) <<< fail $ "Expected " <> show l
  lambda = lexeme Lambda
  dot = lexeme Dot
  openParen = lexeme OpenParen
  closeParen = lexeme CloseParen
  identifier = try $ do
    Token t <- nextToken
    case t.lexeme of
      Identifier id -> pure id
      _ -> fail "Expected an identifier"

expression :: Parser (List Token) Expr
expression = fix expr

showExpr :: Expr -> String
showExpr = s
  where
  s (Abstraction x b) = abstraction x b
  s (Variable x) = x
  s (Application f x) = application (function f) (showFinal x)
  abstraction x b = "\\" <> x <> "." <> s b
  application f x = f <> " " <> x
  function (Application g y) = application (function g) (showAtom y)
  function ex = showAtom ex
  showAtom (Variable y) = y
  showAtom ex = "(" <> s ex <> ")"
  showFinal (Abstraction x b) = abstraction x b
  showFinal ex = showAtom ex

data ParserQuery a = Parse (List Token) (Expr -> a)
newtype ParserComponentState = ParserComponentState
  { parseError :: Maybe ParseError, lastExpression :: Maybe Expr }

derive instance ntParserComponentState :: Newtype ParserComponentState _

parseQuery ::
  forall a action slots output m.
  ParserQuery a ->
  HalogenM ParserComponentState action slots output m (Maybe a)
parseQuery (Parse toks onParse) = do
  case runParser toks (expression <* notFollowedBy nextToken) of
    Left pe -> do
      modify_ (over ParserComponentState (_ { parseError = Just pe }))
      pure Nothing
    Right ex -> do
      put $ ParserComponentState { parseError: Nothing, lastExpression: Just ex }
      pure $ Just $ onParse ex

parserComponent :: forall input output m. Component ParserQuery input output m
parserComponent = mkComponent
  { initialState: const $ ParserComponentState
      { parseError: Nothing, lastExpression: Nothing }
  , render
  , eval: mkEval
      { initialize: Nothing
      , receive: const Nothing
      , handleAction: const (pure unit)
      , handleQuery: parseQuery
      , finalize: Nothing
      }
  }
  where
  render (ParserComponentState st) = div [ class_ $ ClassName "parser" ] $
    maybe []
      (\pe -> pure $ overlay [ ClassName "parser_error" ] [ text (show pe) ])
      st.parseError
      <> maybe [] (\ex -> pure $ displayExpr ex) st.lastExpression

displayExpr :: forall slot action. Expr -> HTML slot action
displayExpr = flowTree step
  where
  next (Application f x) = [ Tuple Nothing f, Tuple Nothing x ]
  next (Variable _) = []
  next (Abstraction _x b) = [ Tuple Nothing b ]
  step ex = MkTreeF { rootContent: [ text $ showExpr ex ], links: next ex }
